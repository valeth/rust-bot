use serde_yaml::Error;

#[derive(Serialize, Deserialize, Debug)]
pub struct Params {
    pub token: String,
    pub owners: Vec<u64>
}

impl Params {
    pub fn set() -> Result<Self, Error> {
        let params: &str = include_str!("params.yml");
        let parameters: Self = serde_yaml::from_str(params)?;
        Ok(parameters)
    }
}