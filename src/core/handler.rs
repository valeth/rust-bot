use std::collections::VecDeque;
use crate::core::config::Config;
use serenity::model::guild::Member;
use serenity::model::id::GuildId;

extern crate serenity;
use serenity::{
    model::{channel::Message, gateway::Ready},
    prelude::{Context, EventHandler},
};

pub struct Handler {
    pub cfg: Config,
}

impl EventHandler for Handler {
    fn message(&self, ctx: Context, msg: Message) {
        let content = msg.content.clone();
        if content.starts_with(&self.cfg.prefix) {
            let mut args: VecDeque<String> = content.split_whitespace().map(str::to_string).collect();;
            let cmd = &args.pop_front().unwrap()[self.cfg.prefix.len()..];
            let cmds = &self.cfg.commands;
            if cmds.contains_key(cmd) {
                let location = match &msg.guild() {
                    Some(gld) => format!("SRV: {}", &gld.read().name.to_owned()),
                    None => "DIRECT MESSAGE".to_string()
                };
                info!("[{}] CMD: {} | {} | USR: {}", msg.timestamp.format("%b%e %T"), cmd, location, msg.author.name);
                let func = cmds.get::<str>(&cmd).unwrap();
                func(ctx, msg, args);
            };
        }
    }
    fn ready(&self, _: Context, ready: Ready) {
        println!("Connected as {} [{}]", ready.user.name, ready.user.id);
    }
    // fn guild_member_addition(&self, _: Context, guild_id: GuildId, member: Member) {
    //     if let Ok(guild) = guild_id.to_partial_guild() {
    //         let channels = guild.channels()
    //             .unwrap();
    //         let channel_search = channels.values()
    //             .find(|c| c.name == "join-log");
    //         if let Some(channel) = channel_search {
    //             let user = member.user.read();
    //             let _ = channel.send_message(|m| m
    //                 .embed(|e| {
    //                     let mut e = e
    //                         .author(|a| a.icon_url(&user.face()).name(&user.name))
    //                         .title("Member Join");
    //                     if let Some(ref joined_at) = member.joined_at {
    //                         e = e.timestamp(joined_at);
    //                     }
    //                     e
    //                 }));
    //         }
    //     }
    // }
}