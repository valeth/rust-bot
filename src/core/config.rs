use std::collections::HashMap;
use std::collections::VecDeque;
use walkdir::WalkDir;

use serenity::{
    model::channel::Message,
    prelude::Context,
};


use crate::commands::{
    choose::choose,
    ping::ping,
    test::test
};

type Callback = fn(ctx: Context, msg: Message, args: VecDeque<String>);

pub struct Config {
    pub commands: HashMap<String, Callback>,
    pub prefix: String
}

impl Config {
    pub fn init(&mut self){
        for e in WalkDir::new("src/commands").into_iter().filter_map(|e| e.ok()) {
            if e.metadata().unwrap().is_file() {
                let file_name = e.path().file_stem().unwrap().to_str().unwrap();
                if file_name != "mod" {
                    let pointer = match file_name {
                        "choose" => choose,
                        "ping" => ping,
                        "test" => test,
                        _ => panic!("No command found for file {:?}", file_name)
                    };
                    let func: Callback = pointer;
                    self.commands.insert(file_name.to_string(), func);
                    // info!("Loaded Command: {}", name)
                }
            }
        }
    }
}