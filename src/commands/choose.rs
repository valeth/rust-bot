use rand::Rng;
use std::iter::FromIterator;
use std::collections::VecDeque;
use serenity::prelude::Context;
use serenity::model::channel::Message;

pub fn choose(_ctx: Context, msg: Message, args: VecDeque<String>) {
    let response = if args.len() != 0 {
        let opt_vec: Vec<String> = Vec::from_iter(args);
        let options = opt_vec.join(" ");
        let choices: Vec<&str> = options.split("; ").collect();
        let choice = rand::thread_rng().choose(&choices).unwrap();
        "I choose... ".to_string() + choice
    } else {
        "Nothing inputted.".to_string()
    };
    let _ = msg.channel_id.say(response);
}